#!/usr/bin/env perl6

use v6;
use JSON::Tiny;

my Str $key;
my $path = './json/example.json';

my $fh = open $path, :r;
my $json = $fh.slurp;
$fh.close;

my %setup = from-json($json);

for %setup -> (:$key, :$value) {
    if %setup{$key}<group>:exists {
        say $key ~ " -> this is group with " ~ %setup{$key}<group>.elems ~ " items:";
        for %setup{$key}<group>.pairs -> %gr {
            my $k = (%gr<>:k).Str;
            say "\t" ~ $k ~ ":";
            for %gr{$k}.pairs -> %s {
                say "\t\t" ~ (%s<>:k).Str ~ ":" ~ (%s<>:v).Str;
            }
        }
    } else {
        say $key;
        for %setup{$key}.pairs ->%s {
             say "\t" ~ (%s<>:k).Str ~ ":" ~ (%s<>:v).Str;
        }
    }
}