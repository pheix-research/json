# Perl6 settings via JSON

## Overview

I have to store setting in XML at Perl5 Pheix. At Perl6 version I've decided to use JSON, so here's conversion script from XML to JSON in Perl5 & JSON parsing script in Perl6.

## License information

These scripts are free and opensource software, so you can redistribute them and/or modify them under the terms of the Artistic License 2.0.

## Links and credits

[Pheix brew site](https://perl6.pheix.org)

[JSON::Tiny in Perl 6](https://github.com/moritz/json)

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

## Contact

Please contact us via [feedback form](https://pheix.org/feedback.html) at pheix.org
